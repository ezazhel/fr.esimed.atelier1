package tp1.test

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.function.Executable
import tp1.levenshteinDistance

class TestToolsTest {
    @Test
    fun levenshteinDistanceTest() {
        Assertions.assertAll(
            Executable {
                Assertions.assertEquals(
                    6,
                    levenshteinDistance("Arnaud", "Julien")
                )
            },
            Executable {
                Assertions.assertEquals(
                    1,
                    levenshteinDistance("Julie", "Julien")
                )
            }
        )
    }
}
