package tp1

fun levenshteinDistance(string1:CharSequence, string2:CharSequence): Int{
    val string1Lenght = string1.length
    val string2Lenght = string2.length
    var d = Array(string1Lenght+1) { IntArray(string2Lenght+1)}
    for(i in 0..string1Lenght)
    {
        d[i][0]=i
    }
    for(j in 0..string2Lenght)
    {
        d[0][j]=j
    }
    for(i in 1..string1Lenght)
    {
        for(j in 1..string2Lenght)
        {
            val cost = if(string1[i-1] == string2[j-1]) 0 else 1
            d[i][j] = minOf(d[i-1][j]+1, d[i][j-1]+1, d[i-1][j-1]+cost)
        }
    }
    return d[string1Lenght][string2Lenght]
}